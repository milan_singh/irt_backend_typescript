import './sequelize'

import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';
import * as koaLogger from 'koa-logger';
import * as koaBodyParser from 'koa-bodyparser';
import * as koaCors from '@koa/cors';

import * as userRoutes from './routes/user'

const app = new Koa();
const koaRouter = new KoaRouter();
const options = {
  credentials : true
}

app
.use(koaBodyParser())
.use(koaCors(options))
.use(koaLogger())
.use(koaRouter.routes());

koaRouter.post('/user-register', userRoutes.userRegister)

koaRouter.post('/user-login', userRoutes.userLogin)

koaRouter.post('/user-logout', userRoutes.userLogout)

app.listen(3000);
console.log('server started on http://localhost:3000/');