
import * as bcrypt from 'bcryptjs'
import * as jwt from 'jsonwebtoken'

import { IMiddleware } from 'koa-router'

import User from './../models/user'

import { AUTH_KEY } from '../config'

const userRegister: IMiddleware = async (ctx, next) => {
  // validate for empty input code is to be written
  const { email, name, password, role, otp } = ctx.request.body
  const userSearchResult = await findSingleUserInDbByEmail(email)
  if (userSearchResult) {
		ctx.response.status = 400
		ctx.body = { message: 'User already exists.' }
		return
  }
    
  const newUser = new User(ctx.body)
  const hashPassword = bcrypt.hashSync(password, 10)
  const userInsertResult = await User.build({
    name: name,
    email: email,
    password: hashPassword,
    role: role
  })
  .save()

  if (userInsertResult) {
		ctx.body = { message: 'User registered successfully.' }
    return 
  }
}

const userLogin: IMiddleware = async (ctx, next) => {
	const { email, password } = ctx.request.body
	const userSearchResult = await findSingleUserInDbByEmail(email)
	if (!userSearchResult) {
		ctx.response.status = 400
		ctx.body = { message: 'Please registered your account first.' }
		return
	}
	const dbPassword = userSearchResult.password
	const userId = userSearchResult.id
	const isPasswordSame = comparedWithDbPassword(password, dbPassword)
	if (isPasswordSame) {
		const jwtToken = jwt.sign({
			email: email,
			id: userId
		}, AUTH_KEY)
		ctx.response.status = 200
		ctx.cookies.set('access_token', jwtToken, {
			httpOnly: true,
			secure: false
		})
		ctx.body = { access_token: jwtToken } 
		return
	}
}

const userLogout: IMiddleware = (ctx, next) => {
	ctx.cookies.set('access_token')
	ctx.body = { status: false, message: 'Logout successfully' }
	return
}

function findSingleUserInDbByEmail (email: string) {
	return User.findOne({
		raw: true,
		where: {
			email: {
					$iLike: email
			}
		}
	})
}

function comparedWithDbPassword (inputPassword: string, dbPassword: string) {
	if (bcrypt.compareSync(inputPassword, dbPassword)) {
		return true
	}
	return false
}

export {
  userRegister,
  userLogin,
  userLogout
}