import { Table, Column, Model } from 'sequelize-typescript';

@Table({
  tableName: 'users'
})
export default class User extends Model<User> {
  @Column
  name!: string;

  @Column
  email!: string;

  @Column
  password!: string;

  @Column
  jwt_token!: string;

  @Column
  role! : string;

  @Column
  otp!: number;

  @Column
  otp_expires_in!: string;

  @Column
  createdBy!: number;

  @Column
  createdAt!: string;

  @Column
  status!: number;
}