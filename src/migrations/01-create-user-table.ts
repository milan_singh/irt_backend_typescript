import * as Sequelize from 'sequelize'

export async function up(sequelize: Sequelize.Sequelize) {
  const queryInterface = sequelize.getQueryInterface()
  
  await queryInterface.createTable('users', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    email: {
      allowNull: false,
      type: Sequelize.STRING
    },
    password: {
      allowNull: false,
      type: Sequelize.STRING
    },
    jwt_token: {
      type: Sequelize.STRING
    },
    role: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    otp: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    otp_expires_in: {
      type: Sequelize.DATE
    },
    createdBy: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    createdAt: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()')
    },
    updatedAt: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('NOW()')
    },
    status: {
      type: Sequelize.INTEGER,
      comment: '1-> Active, 0-> Inactive',
      defaultValue: 1
    }
  });
}

export async function down(sequelize: Sequelize.Sequelize) {
  const queryInterface = sequelize.getQueryInterface();
  await queryInterface.dropTable('users');
}